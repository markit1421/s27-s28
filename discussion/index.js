const protocol = require('http'); 

let port = 3000; 

protocol.createServer((req, res) => {
     res.write('Welcome to the Server');
     res.end(); 
}).listen(port); 

console.log(`Server is on port ${port}`); 
